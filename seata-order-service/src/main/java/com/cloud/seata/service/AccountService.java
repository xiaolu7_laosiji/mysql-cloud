package com.cloud.seata.service;

import com.cloud.demo2020.common.ResultBean;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;

@FeignClient("seata-account-service")
public interface AccountService {
    @PostMapping(value = "/account/decrease")
    ResultBean decrease(@RequestParam("userId") long userId, @RequestParam("money") BigDecimal money);
}
