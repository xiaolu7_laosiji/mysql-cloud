package com.cloud.seata.service;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.cloud.seata.entity.Order;
import com.cloud.seata.mapper.OrderMapper;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderService {
    @Autowired
    private AccountService accountService;
    @Autowired
    private StorageService storageService;
    @Autowired
    private OrderMapper orderMapper;

    @GlobalTransactional
    public void create(Order order) {
        order.setStatus((short)0);
        this.orderMapper.insert(order);
        this.storageService.decrease(order.getProductId(), order.getNum());
        this.accountService.decrease(order.getUserId(), order.getMoney());
        //update t_order set status =1 where user_id =#{userId} and status=#{status}
        order.setStatus((short) 1);
        this.orderMapper.updateById(order);
    }
}
