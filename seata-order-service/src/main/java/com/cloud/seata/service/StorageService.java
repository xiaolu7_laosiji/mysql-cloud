package com.cloud.seata.service;

import com.cloud.demo2020.common.ResultBean;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient("seata-storage-service")
public interface StorageService {
    @PostMapping(value = "/storage/decrease")
    ResultBean decrease(@RequestParam("productId") long productId, @RequestParam("num") int num);
}
