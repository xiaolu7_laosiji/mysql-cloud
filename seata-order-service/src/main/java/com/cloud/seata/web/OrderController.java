package com.cloud.seata.web;

import com.cloud.demo2020.common.ResultBean;
import com.cloud.seata.entity.Order;
import com.cloud.seata.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/order")
public class OrderController {
    @Autowired
    private OrderService orderService;

    @RequestMapping("/create")
    public ResultBean create(Order order) {
        this.orderService.create(order);
        return new ResultBean(1);
    }
}
