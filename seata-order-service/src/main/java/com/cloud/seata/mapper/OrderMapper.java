package com.cloud.seata.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cloud.seata.entity.Order;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OrderMapper extends BaseMapper<Order> {
   int updateStatus(Order order);
}
