package com.cloud.seata.web;

import com.cloud.demo2020.common.ResultBean;
import com.cloud.seata.mapper.StorageMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/storage")
public class StorageController {

    @Autowired
    private StorageMapper storageMapper;
    @PostMapping("/decrease")
    public ResultBean decrease(long productId, int num) {
        return new ResultBean(this.storageMapper.decrease(productId, num));
    }
}
