package com.cloud.seata.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface StorageMapper {
    int decrease(@Param("productId") long productId, @Param("count") int count);
}
