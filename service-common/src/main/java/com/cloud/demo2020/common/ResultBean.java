package com.cloud.demo2020.common;

import java.io.Serializable;

public class ResultBean implements Serializable {
    private int code = 200;
    private String msg = "操作成功";
    public Object data;
    private boolean success = true;
    public ResultBean() {

    }

    public ResultBean(Object data) {
        this.data = data;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public ResultBean(boolean success, String msg) {
        this.success = success;
        this.msg = msg;
    }
}
