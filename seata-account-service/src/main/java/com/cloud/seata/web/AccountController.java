package com.cloud.seata.web;

import com.cloud.demo2020.common.ResultBean;
import com.cloud.seata.entity.Account;
import com.cloud.seata.mapper.AccountMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

@RestController
@RequestMapping("/account")
public class AccountController {
    @Autowired
    private AccountMapper accountMapper;

    @PostMapping("/decrease")
    public ResultBean decrease(long userId, BigDecimal money) {
        return new ResultBean(this.accountMapper.decrease(userId, money));
    }
}
