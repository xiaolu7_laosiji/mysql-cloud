package com.cloud.seata.mapper;

import com.cloud.seata.entity.Account;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;

@Mapper
public interface AccountMapper {
    int decrease(@Param("id") long id, @Param("money") BigDecimal money);
}
