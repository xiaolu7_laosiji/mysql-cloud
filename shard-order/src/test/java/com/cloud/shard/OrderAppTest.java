package com.cloud.shard;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.cloud.shard.entity.COrder;
import com.cloud.shard.mapper.COrderMapper;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Random;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = OrderApp.class)
public class OrderAppTest {

    @Autowired
    private COrderMapper cOrderMapper;

    @Test
    public void writeTest() {
        for (int i = 0; i < 30; i++) {
            COrder cOrder = new COrder();
            //cOrder.set
            cOrder.setDel(false);
            cOrder.setUserId(new Random().nextInt(100000));
            cOrder.setCompanyId(100000001);
            cOrder.setPublishUserId(200000001);
            cOrder.setPositionId(30000001);
            cOrder.setResumeType((short)0);
            cOrder.setStatus("WAIT");
            cOrder.setCreateTime(new Date());
            cOrder.setUpdateTime(new Date());
            this.cOrderMapper.insert(cOrder);
        }
    }

    @Test
    public void readTest() {
        List<COrder> list = this.cOrderMapper.selectList(new QueryWrapper<>());
        Assert.assertEquals(list.size(), 30);
    }
}
