package com.cloud.shard.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cloud.shard.entity.COrder;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface COrderMapper extends BaseMapper<COrder> {
}
